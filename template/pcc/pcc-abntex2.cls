%%
%% Customizações do abnTeX2 (http://abnTeX2.googlecode.com) para o:
%%
%% Programa de Pós-graduação em Ciência da Computação
%% Departamento de Informática
%% Centro de Tecnologia
%% Universidade Estadual de Maringá
%%
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either version 1.3
%% of this license or (at your option) any later version.
%% The latest version of this license is in
%%   http://www.latex-project.org/lppl.txt
%% and version 1.3 or later is part of all distributions of LaTeX
%% version 2005/12/01 or later.
%%
%% This work has the LPPL maintenance status `maintained'.
%% 
%% The Current Maintainer of this work is SEU_NOME, SEU_EMAIL
%%
%% Further information about abnTeX2 are available on http://abntex2.googlecode.com/
%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{template/pcc/pcc-abntex2}

\DeclareOption*{
    \PassOptionsToClass{\CurrentOption}{template/abntex2/abntex2}
}
\ProcessOptions\relax
\LoadClass{template/abntex2/abntex2}

% ----------------------------------------------------------
% TIPOGRAFIA
% ---------------------------------------------------------- 
% \renewcommand{\ABNTEXchapterfont}{\rmfamily}          % fonte serifada para títulos
\renewcommand{\ABNTEXchapterfont}{\bfseries\sffamily}   % fonte sem serifa para títulos e em negrito

% ----------------------------------------------------------
% CAPA - DISSERTAÇÃO
% ----------------------------------------------------------
\renewcommand{\imprimircapa}{
    \begin{capa}
        \center
        
        \imprimirinstituicao

        \vfill
        
        \MakeUppercase{\imprimirautor}
        
        \vfill
        
        \begin{center}
            \ABNTEXchapterfont\bfseries\LARGE\imprimirtitulo\normalfont
        \end{center}
        
        \vfill
        \vfill
        
        \imprimirlocal
        \par
        \imprimirdata
    \end{capa}
}

% ----------------------------------------------------------
% CAPA - QUALIFICAÇÃO
% ----------------------------------------------------------
% \renewcommand{\imprimircapa}{
%     \begin{capa}
%         \center
        
%         \imprimirinstituicao

%         \vfill
        
%         \Large Projeto de Disserta\c{c}\~{a}o de Mestrado\normalsize
        
%         \vfill
        
%         \begin{center}
%             \ABNTEXchapterfont\bfseries\LARGE\imprimirtitulo\normalfont
%         \end{center}
        
%         \vfill
        
%         Aluno: \imprimirautor\par
%         \imprimirorientadorRotulo~\imprimirorientador\par
%         \abntex@ifnotempty{\imprimircoorientador}{
%             \imprimircoorientadorRotulo~\imprimircoorientador
%         }
        
%         \vfill
%         \vfill
        
%         \imprimirlocal
%         \par
%         \imprimirdata
%     \end{capa}
% }

% ----------------------------------------------------------
% FOLHA DE ROSTO
% ----------------------------------------------------------
\makeatletter
\renewcommand{\folhaderostocontent}{
    \center

    \MakeUppercase{\imprimirautor}

    \vfill
    \vfill
    \vfill
    
    \begin{center}
        \ABNTEXchapterfont\bfseries\Large\imprimirtitulo\normalfont
    \end{center}
    
    \vfill

    \abntex@ifnotempty{\imprimirpreambulo}{
        \hspace{.49\textwidth}
        \begin{minipage}{.5\textwidth}
            \SingleSpacing
            \imprimirpreambulo
        \end{minipage}
        
        \hspace{.49\textwidth}
        \begin{minipage}{.5\textwidth}
            \SingleSpacing
            \imprimirorientadorRotulo~\imprimirorientador\par
            \abntex@ifnotempty{\imprimircoorientador}{
                \imprimircoorientadorRotulo~\imprimircoorientador
            }
        \end{minipage}
    }
    
    \vfill
    \vfill

    \imprimirlocal
    \par
    \imprimirdata
}
\makeatother

% ----------------------------------------------------------
% CABEÇALHO
% ----------------------------------------------------------
% remove o nome do capítulo e a linha horizontal do cabeçalho
\makeevenhead{abntheadings}{\ABNTEXfontereduzida\thepage}{}{}
\makeoddhead{abntheadings}{}{}{\ABNTEXfontereduzida\thepage}
\makeheadrule{abntheadings}{0pt}{0pt}