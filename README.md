# Gerar seu pdf completo incluindo abreviações e índices
make 

#Gerar pdf sem atualizar (Rápido) índices e abreviações
make main

#Excluir arquivos intermediários e manter apenas o essencial
make clean 